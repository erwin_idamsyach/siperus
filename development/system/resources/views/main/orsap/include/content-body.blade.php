<div class="content-wrapper min-height" style="min-height:1px;">
	<section class="content-header">
		<h1>
			@yield('menu')
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			@foreach($breadcrumb as $val)
				<li>{{$val}}</li>
			@endforeach
		</ol>
	</section>
	<section class="content">
		<div class="page-ajax">
			<div class="row" style="margin-bottom:15px;">
				<div class="col-md-12">
					@yield('content-filter')
				</div>
			</div>
		</div>
		<div class="row @yield('box-dpd')">
			<div class="col-md-12">
				<div class="panel">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-md-2 col-sm-3 col-xs-6">
								<h4>@yield('box-header-dpd')</h4>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-6 pull-right hide ">
								<div class="@yield('add-button','hide') btn-block  btn-warning btn" data-toggle="modal" data-target="#modal-input-struk" onclick="actionTambah()"><i class="fa fa-plus"></i> Tambah</div>
							</div>
							<div class="@yield('download-button','show') col-md-2 col-sm-3 col-xs-6 pull-right">
								<div class="btn-warning btn-block btn" onclick="actionDownload('excel','verifikasi','DPD','')"><i class="fa fa-print"></i> Excel</div>
							</div>
							<div class="@yield('download-button','show') col-md-2 col-sm-3 col-xs-6 pull-right">
								<div class="btn-warning btn-block btn" onclick="actionDownload('pdf','verifikasi','DPD','')"><i class="fa fa-print"></i> PDF</div>
							</div>
						</div>
					</div>				
					<div class="box-body table-responsive">
						<table class="table table-hover table-striped " id="table">
							<thead>
								@yield('table-header-dpd')
							</thead>
							<tbody>
								@yield('table-body-dpd')
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
function verifikasiDownload(jenis,prov){
		window.location = '{{ asset("download/verifikasi") }}?jenis='+jenis+'&prov='+prov;
		// window.location = '{{ url() }}/system/resources/views/main/download/verifikasi.php?jenis='+jenis+'&prov='+prov;
	}
function actionEdit(targetId,bioId,obj) {
	$('#modal-input-struk').modal('show');
	jQuery('#form-input-struk').attr('action',"{{url()}}/@yield('action-edit')"+targetId);
	jQuery('#submiter').text('Edit');
	jQuery('#my-modal-label').text('Edit @yield('modal_struk_tipe')');
	@yield('content_action_edit_func')
}
function actionTambah(){
	jQuery('#form-input-struk').attr('action',"{{url()}}/@yield('action-tambah')");
	jQuery('#submiter').text('Tambah');
	jQuery('#my-modal-label').text('Tambah @yield('modal_struk_tipe')');
}
function detailVerif(id){
	window.location.href='{{ url().'/verifikasi/detail' }}/'+id;
}
$(function(){	
	$('#prov').change(function(){
		var provId = $(this).val();
		@yield('goto_prov')
		changeKabupatenOptionKPU(null,'#kab',provId);

	});
});
</script>