@section('table_action')	
<td style="width:1%;white-space:nowrap;">
	<div onclick="getDataDetailVerif('{{ @$val->orsap_id }}')" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Detail"><i class="fa fa-search"></i></div>
	<div onclick="actionEdit({{$val->orsap_id}},{{'{'.(@$obj?:'').'}'}})" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit"><i class="fa fa-edit"></i></div>
	<a href="{{asset('input/hapus/orsap/'.$val->orsap_id)}}" onclick="return confirm('Apakah anda yakin ingin menhapus data ini?');" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
	<div onclick="printUser('{{ @$val->orsap_id }}')" class="hide btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Print"><i class="fa fa-print"></i></div>
	<div onclick="getExcel('{{ @$val->orsap_id }}')" class="hide btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Excel"><i class="fa fa-file"></i></div>
</td>
@overwrite
@section('table_data')
<style type="text/css">
	.fa.fa-check {
		color: green !important;
	}	
	.fa.fa-times {
		color: red !important;
	}
</style>
		<td>{{ $val->orsap_deskripsi?:'-' }}</td>
		<td>@if($val->orsap_aktif == 1) <i class="fa fa-check" aria-hidden="true"></i> @else <i class="fa fa-times" aria-hidden="true"></i> @endif</td>
		<td>@if($val->orsap_kantor == 1) <i class="fa fa-check" aria-hidden="true"></i> @else <i class="fa fa-times" aria-hidden="true"></i> @endif</td>
		<td>@if($val->orsap_pengurus == 1) <i class="fa fa-check" aria-hidden="true"></i> @else <i class="fa fa-times" aria-hidden="true"></i> @endif</td>
		<td>{{ $val->orsap_keterangan?:'-' }}</td>
@overwrite