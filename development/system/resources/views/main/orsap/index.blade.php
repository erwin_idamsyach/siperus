@extends('main.layout.layout')
@section('content')
	<script type="text/javascript" src="{{asset('asset/js/jquery-3.1.1.js ')}}"></script>
	<script type="text/javascript" src="{{asset('asset/js/AhmadApp.js ')}}"></script>
	@section('title-page','ORSAP/ORTOM')

	@section('menu','ORSAP/ORTOM<small>List</small>')
	@section('box-header-dpd','List Verifikasi ORSAP/ORTOM TK. DPD')
	
	@section('action-tambah','actions/orsap')
	@section('action-edit','actions/orsap/')
	
	@section('content-filter')
		@section('indo_combo_fprov','initial')
		@include('main.input.section_indo_filter_combo')
	@stop
	@section('goto_prov')
		location.href="{{ asset('orsap') }}/"+provId;
	@stop
	
	@section('table-header-dpd')
		<style type="text/css">
			th {
				text-align: left !important;
			}
		</style>
		<tr>
			<th>No</th>
			<th>Deskripsi</th>
			<th>Aktif</th>
			<th>Kantor</th>
			<th>Pengurus</th>
			<th>Keterangan</th>
			<th>Action</th>
		</tr>
	@stop	

	@section('table-body-dpd')
		{{--*/$no =1/*--}}
		{{--*/$bio_xxx_id='bio_dpd_id'/*--}}
		{{--*/$type='dpd'/*--}}
		  @foreach($dataOrsap as $val)
		   <?php
			$obj='orsap_id:"'.$val->orsap_id.
			'",orsap_deskripsi:"'.$val->orsap_deskripsi.
			'",orsap_aktif:"'.$val->orsap_aktif.
			'",orsap_kantor:"'.$val->orsap_kantor.
			'",orsap_pengurus:"'.$val->orsap_pengurus.
			'",orsap_keterangan:"'.$val->orsap_keterangan.'"';

			?>
		  @include('main.orsap.include.content-table')
		  <tr>
		  <td>{{ $no++ }}</td>
			@yield('table_data')		
			@yield('table_action')
		  </tr>
		  @endforeach
	@stop
	
	@section('content_action_edit_func')
		$("#namaDPC").val(obj.ver_nama)
		$("#alamatKantor").val(obj.ver_alamat)
		
		$("#jumlahKTA").val(obj.ver_jumlah)
		$("#nomerRekening").val(obj.ver_nomer)

		if(obj.ver_status_kantor == '1') {
			$("#statusKantor1").attr('checked',true);
		} else if(obj.ver_status_kantor == '2') {
			$("#statusKantor0").attr('checked',true);
		} else {
			/* $("#statusKantor1").attr('checked',false);
			$("#statusKantor0").attr('checked',false); */
		}

		$("input[id=statusKantor"+obj.ver_status_kantor+"]").attr('checked',true);
		
		if(obj.ver_staf == '1')
			$("#stafAdmin1").attr('checked',true);
		if(obj.ver_staf == '0')
			$("#stafAdmin0").attr('checked',true);
	@stop
	
	@include('main.orsap.include.content-body')
	@include('main.orsap.include.modal')
	@include('main.orsap.include.modal-detail')
@stop
