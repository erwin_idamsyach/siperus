@extends('main.layout.layout')
@section('content')
	<script type="text/javascript" src="{{asset('asset/js/jquery-3.1.1.js ')}}"></script>
	<script type="text/javascript" src="{{asset('asset/js/AhmadApp.js ')}}"></script>
	@section('title-page','Verifikasi')


	@section('menu','Verifikasi<small>List</small>')
	@section('box-header','List Verifikasi')
	
	@include('main.verifikasi.include.content-body')
@stop
