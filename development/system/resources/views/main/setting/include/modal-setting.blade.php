<!-- Modal Hak Akses -->
<div class="modal fade" id="modalTambahHakAkses" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header modal-header-default modal-danger">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div class="modal-title" id="myModalLabel">Tambah Data</div>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<form action="{{ asset('setting/tambah/akses') }}" method="POST">
					<div class="form-group">
						<label>Hak Akses</label>
						<input class="form-control" id='data' name="data"></input>
					</div>
					<div class="form-group">
						<label>Deskripsi</label>
						<textarea class="form-control" id='data'  name="deskripsi"></textarea>
					</div>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="submit" id="btn-hakakses" style="display: none"/>
				</form>
			</div>
		</div>
	  </div>						      
	  <div class="modal-footer">
		<button type="button" class="btn btn-defaul-custom color-white" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-defaul-custom color-white" onclick="$('#btn-hakakses').click()">Save changes</button>
	  </div>
	</div>
  </div>
</div>
<!-- /. Modal Hak Akses -->

<!-- Modal Agama -->
<div class="modal fade" id="modalTambahAgama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header modal-header-default modal-danger">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div class="modal-title" id="myModalLabel">Tambah Data</div>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<form action="{{ asset('setting/tambah/agama') }}" method="POST">
					<div class="form-group">
						<label>Agama</label>
						<input class="form-control" id='data' name="data"></input>
					</div>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="submit" id="btn-agama" style="display: none"/>
				</form>
			</div>
		</div>
	  </div>						      
	  <div class="modal-footer">
		<button type="button" class="btn btn-defaul-custom color-white" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-defaul-custom color-white" onclick="$('#btn-agama').click()">Save changes</button>
	  </div>
	</div>
  </div>
</div>
<!-- /. Modal Agama -->

<!-- Modal Identitas -->
<div class="modal fade" id="modalTambahIdentitas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header modal-header-default modal-danger">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div class="modal-title" id="myModalLabel">Tambah Data</div>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<form action="{{ asset('setting/tambah/identitas') }}" method="POST">
					<div class="form-group">
						<label>Nama Identitas</label>
						<input class="form-control" id='data' name="data"></input>
					</div>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="submit" id="btn-identitas" style="display: none"/>
				</form>
			</div>
		</div>
	  </div>						      
	  <div class="modal-footer">
		<button type="button" class="btn btn-defaul-custom color-white" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-defaul-custom color-white" onclick="$('#btn-identitas').click()">Save changes</button>
	  </div>
	</div>
  </div>
</div>
<!-- /. Modal Identitas -->

<!-- Modal JK -->
<div class="modal fade" id="modalTambahJK" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header modal-header-default modal-danger">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div class="modal-title" id="myModalLabel">Tambah Data</div>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<form action="{{ asset('setting/tambah/jk') }}" method="POST">
					<div class="form-group">
						<label>Nama Jenis Kelamin</label>
						<input class="form-control" id='data' name="data"></input>
					</div>
					<div class="form-group">
						<label>Alias Jenis Kelamin</label>
						<input class="form-control" id='data' name="deskripsi"></input>
					</div>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="submit" id="btn-jk" style="display: none"/>
				</form>
			</div>
		</div>
	  </div>						      
	  <div class="modal-footer">
		<button type="button" class="btn btn-defaul-custom color-white" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-defaul-custom color-white" onclick="$('#btn-jk').click()">Save changes</button>
	  </div>
	</div>
  </div>
</div>
<!-- /. Modal JK -->

<!-- Modal Pekerjaan -->
<div class="modal fade" id="modalTambahPekerjaan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header modal-header-default modal-danger">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div class="modal-title" id="myModalLabel">Tambah Data</div>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<form action="{{ asset('setting/tambah/pekerjaan') }}" method="POST">
					<div class="form-group">
						<label>Pekerjaan</label>
						<input class="form-control" id='data' name="data"></input>
					</div>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="submit" id="btn-pekerjaan" style="display: none"/>
				</form>
			</div>
		</div>
	  </div>						      
	  <div class="modal-footer">
		<button type="button" class="btn btn-defaul-custom color-white" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-defaul-custom color-white" onclick="$('#btn-pekerjaan').click()">Save changes</button>
	  </div>
	</div>
  </div>
</div>
<!-- /. Modal Pekerjaan -->

<!-- Modal Status -->
<div class="modal fade" id="modalTambahStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header modal-header-default modal-danger">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div class="modal-title" id="myModalLabel">Tambah Data</div>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<form action="{{ asset('setting/tambah/status') }}" method="POST">
					<div class="form-group">
						<label>Status</label>
						<input class="form-control" id='data' name="data"></input>
					</div>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="submit" id="btn-status" style="display: none"/>
				</form>
			</div>
		</div>
	  </div>						      
	  <div class="modal-footer">
		<button type="button" class="btn btn-defaul-custom color-white" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-defaul-custom color-white" onclick="$('#btn-status').click()">Save changes</button>
	  </div>
	</div>
  </div>
</div>
<!-- /. Modal Status -->