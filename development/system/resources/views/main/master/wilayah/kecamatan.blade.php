@extends('main.layout.layout')

@section('title-page','List Kecamatan')

@section('main-sidebar')
@endsection

@section('content')
<?php $dataUsers = HelperData::getDataUser('idLogin'); ?>
<div class="modal primary" id="tambahKecamatan" role="dialog" aria-labelledby="tambahKecamatanLabel" >
	<div class="modal-dialog modal-md" role="document">
		<form action="{{ asset('proses/tambah/wilayah/kecamatan') }}" name="barangbuktiform" enctype="multipart/form-data" method="post">
			<div class="modal-content">
				<div class="modal-header modal-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title modal-primary" id="myModalLabel">Tambah Kecamatan</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						@if(session('idRole') == 3)
								{{--*/ $statusProvinsi = 'hide' /*--}}
								{{--*/ $statusKab = 'show' /*--}}
						@elseif(session('idRole') == 4)
								{{--*/ $statusProvinsi = 'hide' /*--}}
								{{--*/ $statusKab = 'hide' /*--}}
						@else
								{{--*/ $statusProvinsi = 'show' /*--}}
								{{--*/ $statusKab = 'show' /*--}}
						@endif
						<div class="form-group col-md-12 {{ $statusProvinsi }} ">
							<label for="nama" class="col-md-4">Nama Provinsi</label>
							<div class="col-md-8">
								<select name="id_provinsi" id="id_provinsi" class="form-control">
									<option value="">--- Provinsi ---</option>
									@foreach($dataProv as $tmp)
										<option value="{{$tmp->geo_prov_id}}">{{$tmp->geo_prov_nama}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12 {{ $statusKab }} ">
							<label for="nama" class="col-md-4">Nama Kabupaten</label>
							<div class="col-md-8">
								<select name="id_kabupaten" id="id_kabupaten" class="form-control">
									<option value="">--- Kabupaten ---</option>
									@if(@$selected[0])
										@foreach($dataKab as $tmp)		
											<option value="{{ $tmp->geo_kab_id }}">{{ $tmp->geo_kab_nama }}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label for="nama" class="col-md-4">Nama Kecamatan</label>
							<div class="col-md-8">
								<input type="text" name="nama_kecamatan" id="nama_kecamatan" class="form-control" placeholder="Nama Kecamatan">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-warning"  id="barangbuktibtn">Simpan</button>
				</div>
			</div>
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		</form>
	</div>
</div>
<div class="modal primary" id="editKecamatan" role="dialog" aria-labelledby="tambahKecamatanLabel" >
	<div class="modal-dialog modal-md" role="document">
		<form action="{{ asset('proses/edit/wilayah/kecamatan') }}" name="barangbuktiform" enctype="multipart/form-data" method="post">
			<div class="modal-content">
				<div class="modal-header modal-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title modal-primary" id="myModalLabel">Edit Kecamatan</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="form-group col-md-12 {{ $statusProvinsi }}">
							<label for="nama" class="col-md-4">Nama Provinsi</label>
							<div class="col-md-8">
								<select name="edit_id_provinsi" id="edit_id_provinsi" class="form-control">
									<option value="">--- Provinsi ---</option>
									@foreach($dataProv as $tmp)
										<option value="{{$tmp->geo_prov_id}}">{{$tmp->geo_prov_nama}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12 {{ $statusKab }}">
							<label for="nama" class="col-md-4">Nama Kabupaten</label>
							<div class="col-md-8">
								<select name="edit_id_kabupaten" id="edit_id_kabupaten" class="form-control">
									<option value="">--- Kabupaten ---</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label for="nama" class="col-md-4">Nama Kecamatan</label>
							<div class="col-md-8">
								<input type="text" name="edit_id_kecamatan" id="edit_id_kecamatan" class="form-control hide" placeholder="Nama Kecamatan">
								<input type="text" name="edit_nama_kecamatan" id="edit_nama_kecamatan" class="form-control" placeholder="Nama Kecamatan">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-warning"  id="barangbuktibtn">Simpan</button>
				</div>
			</div>
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		</form>
	</div>
</div>

<div class="content-wrapper min-height" style="min-height:1px;">
	<section class="content-header">
	  <h1>
		Master Data
		<small>Wilayah</small>
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li>Master Data</li>
		<li>Wilayah</li>
		<li class="active">Kecamatan</li>
	  </ol>
	</section>

	<section class="content">
		<div class="page-ajax">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
					   <div class="response-cari">
						  <!-- /.box-tools -->
						  <?php $no=1; ?>
						  <div class="box-header with-border">
							  <div class="row">
								<div class="col-md-2 col-sm-3 col-xs-6">
									List Kecamatan
								</div>
								<div class="col-md-2 col-sm-3 col-xs-6 pull-right">
									@if(session('statActHide') != 1)
									<div onclick="addKec({{session('idProvinsi')}})" class="btn-block btn-warning btn" data-toggle="modal" data-target="#tambahKecamatan"><i class="fa fa-plus"></i> Tambah</div>
									<div id="btnEditKecamatan" class="btn-block btn-warning btn hide" data-toggle="modal" data-target="#editKecamatan"><i class="fa fa-plus"></i> Tambah</div>
									@endif
								</div>
							  </div>
						  </div>
						  <div class="panel-body">
							<div class="row" id="canvasFilter">
								<div class="col-md-2 col-sm-6 col-xs-12 {{ $statusProvinsi }}">
									<select class="form-control custom-field-litle" id="prov" name="prov" required">
										<option>--- Provinsi ---</option>
										@foreach($dataProv as $tmp)									
											<option value="{{ $tmp->geo_prov_id }}"> {{ $tmp->geo_prov_nama }} </option>
										@endforeach
									</select>
								</div>
								<div class="col-md-2 col-sm-6 col-xs-12 {{ $statusKab }}">
									<select class="form-control custom-field-litle" id="kab" name="kab" required>
										<option value="">--- Kabupaten ---</option>
									</select>
								</div>
							</div>
							<table class="table table-striped table-hover">
							  <thead>
								<tr>
								  <th>No</th>
								  <th>Nama Kecamatan</th>
								  <th width="150">Aksi</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php $no =1; ?>
							  @foreach($data as $tmp)
							  <tr>
								<td>{{ $no++ }}</td> 
								<td>{{ $tmp->geo_kec_nama }}</td>
								<td>
								  <div onclick="getDataDetailWilayah('kec','{{ $tmp->geo_prov_id }}','{{ $tmp->geo_kab_id }}','{{ $tmp->geo_kec_id }}')" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Detail"><i class="fa fa-search"></i></div>
									@if(session('statActHide') != 1)
								  <div onclick='editKecamatan("{{ $tmp->geo_prov_id }}","{{ $tmp->geo_kab_id }}","{{ $tmp->geo_kec_id }}","{{ $tmp->geo_kec_nama }}")' class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit"><i class="fa fa-edit"></i></div>
								  <a href="{{ asset('proses/delete/wilayah/kecamatan/'.$tmp->geo_prov_id.'/'.$tmp->geo_kab_id.'/'.$tmp->geo_kec_id) }}" onclick="return confirm('Apakah anda yakin ingin menhapus data ini?');" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
								  <div onclick="printKecamatan('{{ $tmp->geo_kec_id }}')" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Print"><i class="fa fa-print"></i></div>
									@endif
								</td>
							  </tr>
							  @endforeach
							  </tbody>
							</table>
						  </div>
					   </div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@include('main.master.wilayah.include.modal-detail')
<script src="{{ asset('asset/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script>

$('#prov').change(function(){
	var prov = $(this).val();
	changeKabupatenOptionKPU(this,'#kab',prov);
});
$('#id_provinsi').change(function(){
	var prov = $(this).val();
	changeKabupatenOptionKPU(this,'#id_kabupaten',prov);
});
$('#kab').change(function(){
	var prov = $('#prov').val();
	var kab = $(this).val();
	location.href="<?php echo url(); ?>/master/wilayah/kecamatan/"+prov+"/"+kab;
});
$(document).ready(function(){
	<?php if($prov != 0) { ?>
		changeKabupatenOptionKPU('#prov','#kab','{{ $prov }}');
	<?php } ?>
	<?php if($kab != 0) { ?>
		$('#prov').val('{{ $prov }}');
	<?php } ?>
	@if(session('idKabupaten'))
		$('#id_provinsi').val('{{ session('idProvinsi2') }}');
		$('#id_kabupaten').val('{{ session('idKabupaten') }}');
	@elseif(session('idProvinsi2'))
		$('#id_provinsi').val('{{ session('idProvinsi2') }}');
	@endif
});
function editKecamatan(prov_id,kab_id,kec_id,kec_nama){
	$('#edit_id_provinsi').val(prov_id);
	changeKabupatenOptionKPU('#edit_id_provinsi','#edit_id_kabupaten',prov_id);
	$('#edit_id_kabupaten').val(kab_id);
	$('#edit_id_kecamatan').val(kec_id);
	$('#edit_nama_kecamatan').val(kec_nama);
	$('#btnEditKecamatan').click();
}
</script>
@endsection