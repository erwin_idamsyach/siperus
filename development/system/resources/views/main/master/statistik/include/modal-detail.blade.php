<div class="modal fade" id="modal-view-statistik">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-toggle="modal" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detail Statistik</h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="form-group col-md-12">
							<label for="nama" class="col-md-5">Nama Provinsi</label>
							<label for="nama" class="col-md-1">:</label>
							<div class="col-md-5" id="provins"></div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label for="nama" class="col-md-5">DPC (Kabupaten/Ada)</label>
							<label for="nama" class="col-md-1">:</label>
							<div class="col-md-5" id="kabupaten"></div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12" id="ddpc">
							<label for="nama" class="col-md-5">PAC (Kecamatan/Ada)</label>
							<label for="nama" class="col-md-1">:</label>
							<div class="col-md-5" id="kecamatan"></div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12" id="dranting">
							<label for="nama" class="col-md-5">Ranting (Kelurahan/Ada)</label>
							<label for="nama" class="col-md-1">:</label>
							<div class="col-md-5" id="ranting"></div>
						</div>
					</div>
					<div class="row" rows="2">
						<div class="form-group col-md-12" id="danakranting">
							<label for="nama" class="col-md-5">Anak Ranting (RW/Ada)</label>
							<label for="nama" class="col-md-1">:</label>
							<div class="col-md-5" id="aranting"></div>
						</div>
					</div>
					<div class="row" rows="2">
						<div class="form-group col-md-12" id="dkpa">
							<label for="nama" class="col-md-5">KPA (RT/Ada)</label>
							<label for="nama" class="col-md-1">:</label>
							<div class="col-md-5" id="kpa"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>