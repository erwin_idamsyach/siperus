<?php namespace App\Http\Controllers; 

use DB;
use Input;
use Redirect;

/**
* 
*/
class DashboardController extends Controller
{
	public function getAllStruktur($jenis){
		if($jenis == 'dpd'){
			$dataKabupaten = DB::table('m_geo_prov_kpu')
				->get();
				
			foreach($dataKabupaten as $tmp){				
				$savePendaftaran1 = DB::table('m_struk_dpd')
					->insertGetId([
						'geo_prov_id' => $tmp->geo_prov_id,
						'struk_dpd_nama' => 'Ketua',
						'struk_dpd_created_date' => date('Y-m-d H:i:s'),
						'struk_dpd_created_by' => 1,
						'struk_dpd_status' => 1
					]);
				$savePendaftaran2 = DB::table('m_struk_dpd')
					->insertGetId([
						'geo_prov_id' => $tmp->geo_prov_id,
						'struk_dpd_nama' => 'Sekretaris',
						'struk_dpd_created_date' => date('Y-m-d H:i:s'),
						'struk_dpd_created_by' => 1,
						'struk_dpd_status' => 1
					]);
				$savePendaftaran3 = DB::table('m_struk_dpd')
					->insertGetId([
						'geo_prov_id' => $tmp->geo_prov_id,
						'struk_dpd_nama' => 'Bendahara',
						'struk_dpd_created_date' => date('Y-m-d H:i:s'),
						'struk_dpd_created_by' => 1,
						'struk_dpd_status' => 1
					]);
			}	
		} else if($jenis == 'dpc'){
			$dataKabupaten = DB::table('m_geo_kab_kpu')
				->get();
				
			foreach($dataKabupaten as $tmp){				
				$savePendaftaran1 = DB::table('m_struk_dpc')
					->insertGetId([
						'geo_prov_id' => $tmp->geo_prov_id,
						'geo_kab_id' => $tmp->geo_kab_id,
						'struk_dpc_nama' => 'Ketua',
						'struk_dpc_created_date' => date('Y-m-d H:i:s'),
						'struk_dpc_created_by' => 1,
						'struk_dpc_status' => 1
					]);
				$savePendaftaran2 = DB::table('m_struk_dpc')
					->insertGetId([
						'geo_prov_id' => $tmp->geo_prov_id,
						'geo_kab_id' => $tmp->geo_kab_id,
						'struk_dpc_nama' => 'Sekretaris',
						'struk_dpc_created_date' => date('Y-m-d H:i:s'),
						'struk_dpc_created_by' => 1,
						'struk_dpc_status' => 1
					]);
				$savePendaftaran3 = DB::table('m_struk_dpc')
					->insertGetId([
						'geo_prov_id' => $tmp->geo_prov_id,
						'geo_kab_id' => $tmp->geo_kab_id,
						'struk_dpc_nama' => 'Bendahara',
						'struk_dpc_created_date' => date('Y-m-d H:i:s'),
						'struk_dpc_created_by' => 1,
						'struk_dpc_status' => 1
					]);
			}	
		}
	}
	public function changeToKPU($jenis){
		if($jenis == 'penduduk'){
			$dataPendudukKPU = DB::table('m_penduduk')
				->join('m_geo_kab','m_geo_kab.geo_kab_id','=','m_penduduk.geo_kab_id')
					->whereNull('geo_kab_kpu')
						->get();
			foreach($dataPendudukKPU as $tmp){
				echo $tmp->geo_kab_nama;
				echo '   -   ';
				$cek = DB::table('m_geo_kab_kpu')
					->select('*',DB::raw('REPLACE(geo_kab_nama,"Kab. ","") as kab_name'))
					->where('kab_name','like','"'.$tmp->geo_kab_nama.'"')
						->get();
				echo '   -   ';
				echo count($cek);
				echo '<br />';
			}
		}
	}
	
	public function cekSK($nomer = 0){
		if($nomer == ''){ // view
			return view('cek_sk');
		} else { // Proses Cek
			$count = 0;
			$dataSKAll = [];
			$arrayJenis = ['dpp','dpd','dpc','pac','par','kpa'];
			for($z=0; $z<count($arrayJenis); $z++){
				$jenis = $arrayJenis[$z];
				
				$prosesCek = DB::table('r_bio_'.$jenis)
					->join('m_bio','m_bio.bio_id','=','r_bio_'.$jenis.'.bio_id')
					->join('ref_jk','m_bio.bio_jenis_kelamin','=','ref_jk.jk_id')
					->join('m_struk_'.$jenis,'m_struk_'.$jenis.'.struk_'.$jenis.'_id','=','r_bio_'.$jenis.'.struk_'.$jenis.'_id')
						->where('bio_'.$jenis.'_sk',base64_decode($nomer))
						->where('struk_'.$jenis.'_nama','like','Ketua%')
							->get();
							
				$count = $count+count($prosesCek);
				
				foreach($prosesCek as $tmp){
					$arrayNama = [$tmp->bio_nama_depan,$tmp->bio_nama_tengah,$tmp->bio_nama_belakang];
					$alamat = (isset($tmp->bio_alamat))?"-":$tmp->bio_alamat;
					$dataSK = 
					'<tr>
						<td>Nama</td>
						<td>:</td>
						<td>'.ucwords(strtolower(join($arrayNama,''))).'</td>
					</tr>
					<tr>
						<td>Jenis Kelamin</td>
						<td>:</td>
						<td>'.$tmp->jk_value.'</td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td>'.$alamat.'</td>
					</tr>
					<tr>';
					array_push($dataSKAll,$dataSK);
				}
				
			}			
			
			if($count == 0){
				echo 'kosong';
			} else {
				echo '<table class="table">';
					for($x=0; $x<count($dataSKAll); $x++){
						echo $dataSKAll[$x];
						if($x+1 != count($dataSKAll)){							
							echo '<tr><td colspan="3"><div style="border: 1px solid #b0bec5; max-height: 1px !important;"></div></td></tr>';
						}
					}				
				echo '</table>';
			}
		}
	}
	
	public function getStrukturId($jenis,$ke=0){
		$mulai = ($ke-1)*10000;
		$sampai = $ke*10000;
		
		if($jenis == 'prov') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
							->whereNull('geo_'.$jenis.'_id2')
					->get();			
		} else if($jenis == 'kab') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_geo_kab_kpu.geo_prov_id')
							->whereNull('geo_'.$jenis.'_id2')
						->get();
		} else if($jenis == 'kec') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
				->join('m_geo_kab_kpu','m_geo_kab_kpu.geo_kab_id','=','m_geo_kec_kpu.geo_kab_id')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_geo_kab_kpu.geo_prov_id')
					->whereNull('geo_'.$jenis.'_id2')
						->get();
		} else if($jenis == 'deskel') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
				->join('m_geo_kec_kpu','m_geo_kec_kpu.geo_kec_id','=','m_geo_deskel_kpu.geo_kec_id')
				->join('m_geo_kab_kpu','m_geo_kab_kpu.geo_kab_id','=','m_geo_kec_kpu.geo_kab_id')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_geo_kab_kpu.geo_prov_id')
					->whereNull('geo_'.$jenis.'_id2')
					->skip($mulai)
					->take($sampai)
						->get();
		} else if($jenis == 'rw') {
			$data = DB::table('m_geos_'.$jenis.'_kpu')
				->whereNull('geo_'.$jenis.'_id2')
								->get();
		} else if($jenis == 'tps') {
			$data = DB::table('ref_tps')
				->whereNull('desaId2')
				->groupBy('desaId')
					->skip($mulai)
					->take($sampai)
					->get();
		}
		echo count($data);
		foreach($data as $tmp) {
			$dataUpdate = 0;
			
			$cek = DB::table('m_geo_'.$jenis);
			
			if($jenis == 'prov') {
				$id = $tmp->geo_prov_id;
				$nama = $tmp->geo_prov_nama;
				
			} else if($jenis == 'kab') {
				$provId = $tmp->geo_prov_id2;
				$id = $tmp->geo_kab_id;
				$nama = $tmp->geo_kab_nama;
				
				/* $cek->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id'); */
					/* ->where('m_geo_prov.geo_prov_id',$provId); */
				
			} else if($jenis == 'kec') {
				$provId = $tmp->geo_prov_id2;
				$kabId = $tmp->geo_kab_id2;
				$id = $tmp->geo_kec_id;
				$nama = $tmp->geo_kec_nama;
				
				$cek->join('m_geo_kab','m_geo_kab.geo_kab_id','=','m_geo_kec.geo_kab_id')
					->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
					->where('m_geo_prov.geo_prov_id',$provId)
					->where('m_geo_kab.geo_kab_id',$kabId);
				
			} else if($jenis == 'deskel') {
				$provId = $tmp->geo_prov_id2;
				$kabId = $tmp->geo_kab_id2;
				$kecId = $tmp->geo_kec_id2;
				$id = $tmp->geo_deskel_id;
				$nama = $tmp->geo_deskel_nama;
				
				$cek->join('m_geo_kec','m_geo_kec.geo_kec_id','=','m_geo_deskel.geo_kec_id')
					->join('m_geo_kab','m_geo_kab.geo_kab_id','=','m_geo_kec.geo_kab_id')
					->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
					->where('m_geo_prov.geo_prov_id',$provId)
					->where('m_geo_kab.geo_kab_id',$kabId)
					->where('m_geo_kec.geo_kec_id',$kecId);
				
			} else if($jenis == 'rw') {
				$id = $tmp->geo_rw_id2;
				$nama = $tmp->geo_rw_nama;
			} else if($jenis == 'tps') {
				$id = $tmp->tpsId;
				$nama = $tmp->desaId;
				
				$dataCek = DB::table('m_geo_deskel_kpu')
					->where('geo_deskel_id',$nama)
						->get();
			}
				
			/* $dataCek = $cek->where('geo_'.$jenis.'_nama','like','%'.$nama.'%')
				->get();
			 */
			foreach($dataCek as $tmp){
				if($jenis == 'prov')
					$dataUpdate = $tmp->geo_prov_id; 
				else if($jenis == 'kab')
					$dataUpdate = $tmp->geo_kab_id; 
				else if($jenis == 'kec')
					$dataUpdate = $tmp->geo_kec_id; 
				else if($jenis == 'deskel')
					$dataUpdate = $tmp->geo_deskel_id; 
				else if($jenis == 'tps')
					$dataUpdate = $tmp->geo_deskel_id2; 
			}
			echo $id.' - '.$nama.' - '.$dataUpdate.'<br>';
				$proses = DB::table('ref_tps')
					->where('desaId',$nama)
						->update(['desaId2' => $dataUpdate]);
				/* $proses = DB::table('m_geo_'.$jenis.'_kpu')
					->where('geo_'.$jenis.'_id',$id)
						->update(['geo_'.$jenis.'_id2' => $dataUpdate]); */
			
			
		}
	}
	
	public function getCoordinate($jenis){
		if($jenis == 'prov') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
				->whereNull('geo_'.$jenis.'_lat')
				->orWhereNull('geo_'.$jenis.'_lng')
					->get();			
		} else if($jenis == 'kab') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_geo_kab_kpu.geo_prov_id')
					->whereNull('geo_'.$jenis.'_lat')
					->orWhereNull('geo_'.$jenis.'_lng')
						->get();
		} else if($jenis == 'kec') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
				->join('m_geo_kab_kpu','m_geo_kab_kpu.geo_kab_id','=','m_geo_kec_kpu.geo_kab_id')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_geo_kab_kpu.geo_prov_id')
					->whereNull('geo_'.$jenis.'_lat')
					->orWhereNull('geo_'.$jenis.'_lng')
						->get();
		} else if($jenis == 'deskel') {
			$data = DB::table('m_geo_'.$jenis.'_kpu')
				->join('m_geo_kec_kpu','m_geo_kec_kpu.geo_kec_id','=','m_geo_deskel_kpu.geo_kec_id')
				->join('m_geo_kab_kpu','m_geo_kab_kpu.geo_kab_id','=','m_geo_kec_kpu.geo_kab_id')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_geo_kab_kpu.geo_prov_id')
					->whereNull('geo_'.$jenis.'_lat')
					->orWhereNull('geo_'.$jenis.'_lng')
						->get();
		} else if($jenis == 'rw') {
			$data = DB::table('m_geo_'.$jenis)
				->join('m_geo_deskel_kpu','m_geo_deskel_kpu.geo_deskel_id','=','m_geo_rw.geo_deskel_id')
				->join('m_geo_kec_kpu','m_geo_kec_kpu.geo_kec_id','=','m_geo_deskel_kpu.geo_kec_id')
				->join('m_geo_kab_kpu','m_geo_kab_kpu.geo_kab_id','=','m_geo_kec_kpu.geo_kab_id')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_geo_kab_kpu.geo_prov_id')
					->whereNull('geo_'.$jenis.'_lat')
					->orWhereNull('geo_'.$jenis.'_lng')
						->get();
		}
		
		foreach($data as $tmp){			
			if($jenis == 'prov') {
				$id = $tmp->geo_prov_id;
				$nama = $tmp->geo_prov_nama;
				$namaLokasi = $tmp->geo_prov_nama.', Indonesia';
			} else if($jenis == 'kab') {
				$id = $tmp->geo_kab_id;
				$nama = $tmp->geo_kab_nama;
				$namaLokasi = $tmp->geo_kab_nama.', '.$tmp->geo_prov_nama.', Indonesia';
			} else if($jenis == 'kec') {
				$id = $tmp->geo_kec_id;
				$nama = $tmp->geo_kec_nama;
				$namaLokasi = $tmp->geo_kec_nama.', '.$tmp->geo_kab_nama.', '.$tmp->geo_prov_nama.', Indonesia';
			} else if($jenis == 'deskel') {
				$id = $tmp->geo_deskel_id;
				$nama = $tmp->geo_deskel_nama;
				$namaLokasi = $tmp->geo_deskel_nama.', '.$tmp->geo_kec_nama.', '.$tmp->geo_kab_nama.', '.$tmp->geo_prov_nama.', Indonesia';
			} else if($jenis == 'rw') {
				$id = $tmp->geo_rw_id;
				$nama = $tmp->geo_rw_nama;
				$namaLokasi = $tmp->geo_rw_nama.', '.$tmp->geo_deskel_nama.', '.$tmp->geo_kec_nama.', '.$tmp->geo_kab_nama.', '.$tmp->geo_prov_nama.', Indonesia';
			}
			
			$latlng['api'] = '';
			
			$latlng = $this::getLatLng($namaLokasi);
			
			$update = DB::table('m_geo_'.$jenis.'_kpu')
				->where('geo_'.$jenis.'_id',$id)
					->update([
						'geo_'.$jenis.'_lat' => $latlng['lat'],
						'geo_'.$jenis.'_lng' => $latlng['lng']
					]);
					
		} 
	}
	
	public function viewPage()
	{
		return view('index_page');
	}
	
	public function viewIndex()
	{
		$kontak_dpd = @$_GET['kontak_dpd'];
		$kontak_dpc = @$_GET['kontak_dpc'];
		$suara_tahun = @$_GET['suara_tahun'];
		$agenda_tahun = @$_GET['agenda_tahun'];
		
		$dataDPD = array(); 
		$dataDPC = array(); 
		$dataDPCAll = array(); 
		$dataDPDAll = array(); 
		$dataPengurusAll = array(); 
		
		$jumlahProv = DB::table('m_geo_prov_kpu')->count();
		
		$dataStatistikOrganisasi = DB::table('m_pengurus')
			->select(
				'pengurus_dpc as dpc',
				'pengurus_pac as pac',
				'pengurus_ranting as ranting',
				'pengurus_anak_ranting as anak_ranting',
				'pengurus_kpa as kpa',
				'pengurus_dpc_ada as dpc_ada',
				'pengurus_pac_ada as pac_ada',
				'pengurus_ranting_ada as ranting_ada',
				'pengurus_anak_ranting_ada as anak_ranting_ada',
				'pengurus_kpa_ada as kpa_ada',
				'geo_prov_nama as prov_nama',
				'geo_prov_lat as lat',
				'geo_prov_lng as lng'
			)
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_pengurus.geo_prov_id')
					->groupBy('m_pengurus.geo_prov_id')
						->get(); 
						
		$dataStatistikKursi = DB::table('m_statistik_kursi')
			->select(
				'statistik_dapil_t1 as dapil_t1',
				'statistik_dapil_t2 as dapil_t2',
				'statistik_dapil_t3 as dapil_t3',
				'statistik_kursi_t1 as kursi_t1',
				'statistik_kursi_t2 as kursi_t2',
				'statistik_kursi_t3 as kursi_t3',
				'statistik_kursi_t1_ada as kursi_t1_ada',
				'statistik_kursi_t2_ada as kursi_t2_ada',
				'statistik_kursi_t3_ada as kursi_t3_ada',
				'geo_prov_nama as prov_nama',
				'geo_prov_lat as lat',
				'geo_prov_lng as lng'
			)
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_statistik_kursi.geo_prov_id')
					->groupBy('m_statistik_kursi.geo_prov_id')
						->get(); 
		
		$dataDPDAll = DB::table('m_struk_dpd')
			->select(
				'm_bio.bio_nama_depan as nama',
				'm_bio.bio_telephone as no_telp',
				'm_bio.bio_email as email',
				'm_geo_prov_kpu.geo_prov_nama as prov_nama',
				'm_geo_prov_kpu.geo_prov_lat as lat',
				'm_geo_prov_kpu.geo_prov_lng as lng',
				'm_struk_dpd.struk_dpd_nama as struk_nama'
			)
				->join('r_bio_dpd','r_bio_dpd.struk_dpd_id','=','m_struk_dpd.struk_dpd_id')
				->join('m_bio','m_bio.bio_id','=','r_bio_dpd.bio_id')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','r_bio_dpd.geo_prov_id')
					->where('m_struk_dpd.struk_dpd_nama','like','ketua%')
						->groupBy('r_bio_dpd.geo_prov_id')
							->get(); 
						
		$dataDPCAll = DB::table('r_bio_dpc')
			->select(
				'm_bio.bio_nama_depan as nama',
				'm_bio.bio_telephone as no_telp',
				'm_bio.bio_email as email',
				'm_geo_prov_kpu.geo_prov_nama as prov_nama',
				'm_geo_kab_kpu.geo_kab_nama as kab_nama',
				'm_geo_kab_kpu.geo_kab_lat as lat',
				'm_geo_kab_kpu.geo_kab_lng as lng',
				'r_bio_dpc.geo_kab_id as dpc_kab_id',
				'm_struk_dpc.struk_dpc_nama as struk_nama'
			)
				->join('m_struk_dpc','r_bio_dpc.struk_dpc_id','=','m_struk_dpc.struk_dpc_id')
				->join('m_bio','m_bio.bio_id','=','r_bio_dpc.bio_id')
				->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','r_bio_dpc.geo_prov_id')
				->join('m_geo_kab_kpu','m_geo_kab_kpu.geo_kab_id','=','r_bio_dpc.geo_kab_id')
					->where('m_struk_dpc.struk_dpc_nama','like','ketua%')
						->groupBy('r_bio_dpc.geo_kab_id')
							->get();  
		$type = 'all';
		return view('index', array(
			'jumlahProv' => $jumlahProv,
			'dataDPD' => $dataDPD,
			'dataDPC' => $dataDPC,
			'dataDPDAll' => $dataDPDAll,
			'dataDPCAll' => $dataDPCAll,
			'dataStatistikOrganisasi' => $dataStatistikOrganisasi,
			'dataStatistikKursi' => $dataStatistikKursi,
			'type' => $type
		));
	}

	public function getDataMap($jenis,$jenisPenduduk=0)
	{
		$data = array();
		if($jenis == 'penduduk'){
			if($jenisPenduduk == 'provinsi'){
				$dataPendudukProvinsi = DB::table('m_penduduk')
					->select(DB::raw('*,sum(penduduk_jumlah) as penduduk_jumlah'))
						->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_penduduk.geo_prov_id')
							->groupBy('m_penduduk.geo_prov_id')
								->get();	
				/* $data2 = array(); */
				foreach($dataPendudukProvinsi as $tmp)
				{
					$data1 = array();
					array_push($data1,$tmp->geo_prov_nama,(float)$tmp->geo_prov_lat,(float)$tmp->geo_prov_lng,number_format($tmp->penduduk_jumlah,0, "," , "."));
					array_push($data,$data1);
				}								
			} else if($jenisPenduduk == 'kabupaten'){				
				$dataPendudukKabupaten = DB::table('m_penduduk')
					->select(DB::raw('*,sum(penduduk_jumlah) as penduduk_jumlah'))	
						->join('m_geo_prov_kpu','m_geo_prov_kpu.geo_prov_id','=','m_penduduk.geo_prov_id')
						->join('m_geo_kab_kpu','m_geo_kab_kpu.geo_kab_id','=','m_penduduk.geo_kab_id')
							->groupBy('m_penduduk.geo_kab_id')
								->get();
				/* $data2 = array(); */
				foreach($dataPendudukKabupaten as $tmp)
				{
					$data1 = array();
					array_push($data1,$tmp->geo_kab_nama,$tmp->geo_prov_nama,(float)$tmp->geo_kab_lat,(float)$tmp->geo_kab_lng,number_format($tmp->penduduk_jumlah,0, "," , "."));
					array_push($data,$data1);
				}
			}
			
			/* foreach($dataPendudukKabupaten as $data)
			{
				$data1 = array();
				$latlng = $this::getLatLng($data->geo_kab_nama.','.$data->geo_prov_nama.', Indonesia');
				array_push($data1,$data->geo_kab_nama,$data->geo_prov_nama,$latlng['lat'],$latlng['lng']);
				array_push($data3,$data1);
			} */
			/* 
			$data = array(
				'provinsi' => $data2,
				'kabupaten' => $data3
			); */
		} else if($jenis == 'dapil'){
			$dataDapil = DB::table('m_dapil')
				->where('tingkat_dapil',3)
					->get();
					
			foreach($dataDapil as $tmp){
				$like = SUBSTR($tmp->nama_dapil,0,-2);
				$dataKab = DB::table('m_geo_kab')
					->where('geo_kab_nama','like','%'.$like.'%')
						->first();
				if(count($dataKab) != 0){
					$prosesUpdate = DB::table('m_dapil')
						->where('dapil_id',$tmp->dapil_id)
							->update(['kab_id'=>$dataKab->geo_kab_id]);
				} else {
					echo 'tidak';
				}
				echo '<br>';
			}
		} else {
			
		}
		
		return $data;
	}
	
	public function dataAktif()
	{
		$change = @$_GET['change'];
		$provinsi = DB::table('provinsi')->get();
		echo '<script src="../asset/js/jquery-3.0.0.min.js"></script>';
		echo '<script src="../asset/js/ajaxLokasi.js"></script>';
		if($change == 'kontak_dpd')
		{
			echo '<div class="form-group col-md-12">
				<label for="dpd" class="col-md-2">DPD</label>
				<div class="col-md-4">
					<select name="" id="provinsi" class="form-control">
					<option value="">--- Provinsi ---</option>';
					foreach($provinsi as $prov){
						echo '<option value="'.$prov->id_provinsi.'">'.ucwords(strtolower($prov->Nama_provinsi)).'</option>';
					}
					echo '</select>
				</div> 
				<div class="result col-md-5"></div></div>';
		} else if($change == 'kontak_dpc') {
			echo '<div class="form-group col-md-12">
				<label for="dpd" class="col-md-2">DPC</label>
				<div class="col-md-4">
					<select name="" id="provinsi" class="form-control">
					<option value="">--- Provinsi ---</option>';
					foreach($provinsi as $prov){
						echo '<option value="'.$prov->id_provinsi.'">'.ucwords(strtolower($prov->Nama_provinsi)).'</option>';
					}
					echo '</select>
				</div> 
				<div class="col-md-5">
					<select name="" id="kabupaten" class="form-control">
						<option value="0">--- Kabupaten / Kota ---</option>
					</select>
				</div>
			</div>';
		} else if($change == 'caleg') {
			echo '<div class="form-group col-md-12">
				<label for="dpd" class="col-md-2">Caleg</label>
				<div class="col-md-4">
					<select name="" id="provinsi" class="form-control">
					<option value="">--- Provinsi ---</option>';
					foreach($provinsi as $prov){
						echo '<option value="'.$prov->id_provinsi.'">'.ucwords(strtolower($prov->Nama_provinsi)).'</option>';
					}
					echo '</select>
				</div> 
				<div class="col-md-5">
					<select name="0" id="kabupaten" class="form-control">
						<option value="">--- Kabupaten / Kota ---</option>
					</select>
				</div>
			</div>';
		}
	}
	public function caseMenu()
	{
		$case = @$_GET['case'];
		if($case == 'Caleg') 
		{
			$dataCalon = DB::table('caleg_drh')
				->leftjoin('provinsi','provinsi.id_provinsi','=','caleg_drh.tingkat_provinsi')
				->leftjoin('kabupaten','kabupaten.id_kabupaten','=','caleg_drh.tingkat_kabupaten')
				->leftjoin('surat_keputusan','surat_keputusan.no_sk','=','caleg_drh.no_sk')
 				->leftjoin('caleg_terpilih','caleg_terpilih.caleg_id','=','caleg_drh.id')
				->leftjoin('partai','partai.ID','=','caleg_terpilih.partai_id')
				->where('partai.NAMA_PARTAI','Hanura')
				->where('caleg_drh.id_ketua','')
				->where('caleg_drh.tingkat_provinsi','!=','')
				->groupBy('caleg_drh.nama')
				->select(
					'caleg_drh.nama as nama',
					'caleg_drh.tingkat_provinsi',
					'caleg_drh.tingkat_kabupaten',
					'caleg_drh.jenis_calon',
					'caleg_drh.pekerjaan',
					'caleg_drh.jenis_kelamin',
					'caleg_drh.create_date',
					'caleg_drh.id as caleg_id',
					'caleg_drh.telephone as no_telp',
					'caleg_drh.email as email',
					'caleg_drh.type as type',
					'provinsi.Nama_provinsi',
					'kabupaten.Nama_kabupaten',
					'caleg_drh.no_sk as no_sk',
					'caleg_drh.*'
				)
				->orderBy('kabupaten.Nama_kabupaten','asc')
				->get();
			return response()->view('master_maps', array(
				'dataCalon' => $dataCalon,
				'type' => 'Caleg'
			));
		} else if($case == "DPD") {
			$dataCalon = DB::table('call_center_dpd')
				->leftjoin('provinsi','provinsi.id_provinsi','=','call_center_dpd.provinsi')
				->join('kabupaten','kabupaten.id_provinsi','=','provinsi.id_provinsi')
				->select(
					'call_center_dpd.email as email',
					'call_center_dpd.no_telp as no_telp',
					'call_center_dpd.nama_dpd as nama',
					'call_center_dpd.kabupaten',
					'provinsi.Nama_provinsi',
					'kabupaten.Nama_kabupaten'
				)
				->groupBy('call_center_dpd.nama_dpd')
				->get();
			return response()->view('dpd_maps', array(
				'dataCalon' => $dataCalon,
				'type' => 'DPD'
			));
		} else if($case == "DPC") {
			$dataCalon = DB::table('call_center_dpc')
				->leftjoin('kabupaten','call_center_dpc.kabupaten_kota','=','kabupaten.id_kabupaten')
				->leftjoin('provinsi','kabupaten.id_provinsi','=','provinsi.id_provinsi')
				->groupBy('kabupaten.Nama_kabupaten')
				->groupBy('call_center_dpc.nama_ketua_dpc')
					->select(
					'call_center_dpc.email as email',
					'call_center_dpc.no_telp as no_telp',
					'call_center_dpc.nama_ketua_dpc as nama',
					'call_center_dpc.presentase',
					'provinsi.Nama_provinsi',
					'kabupaten.Nama_kabupaten',
					'call_center_dpc.*'
				)
				->get();
			return response()->view('dpc_maps', array(
				'dataCalon' => $dataCalon,
				'type' => 'DPC'
			));
		} else if($case == 'Survey')  {
			$dataCalon = DB::table('call_center_dpc')
				->join('kabupaten','kabupaten.id_kabupaten','=','call_center_dpc.kabupaten_kota')
				->join('provinsi','provinsi.id_provinsi','=','call_center_dpc.provinsi')
				->where('call_center_dpc.presentase','!=','0')
				->where('call_center_dpc.responden','!=','')
				->select(
					'call_center_dpc.email as email',
					'call_center_dpc.no_telp as no_telp',
					'call_center_dpc.nama_ketua_dpc as nama',
					'call_center_dpc.presentase',
					'provinsi.Nama_provinsi',
					'kabupaten.Nama_kabupaten'
				)->groupBy('kode_dpc')
				->get();
			$dataProvinsi = DB::table('call_center_dpc')
				->join('kabupaten','kabupaten.id_kabupaten','=','call_center_dpc.kabupaten_kota')
				->join('provinsi','provinsi.id_provinsi','=','call_center_dpc.provinsi')
				->where('call_center_dpc.presentase','!=','0')
				->where('call_center_dpc.responden','!=','')
				->groupBy('provinsi.Nama_provinsi')
				->orderBy('provinsi.Nama_provinsi','asc')
				->get();
			return response()->view('survey', array(
				'dataCalon' => $dataCalon,
				'dataProvinsi' => $dataProvinsi,
				'type' => 'survey'
			));
		} else if($case == '0') {
			echo 'kosong';
		}
	}
	public function cariData() 
	{
		$cari = @$_GET['cari'];
		$case = @$_GET['case'];
		if($case == 'Caleg') 
		{
			$dataCalon = DB::table('caleg_drh')
				->join('kabupaten','kabupaten.id_kabupaten','=','caleg_drh.tingkat_kabupaten')
				->join('provinsi','provinsi.id_provinsi','=','kabupaten.id_provinsi')
				->leftjoin('caleg_terpilih','caleg_terpilih.caleg_id','=','caleg_drh.id')
				->join('partai','partai.ID','=','caleg_terpilih.partai_id')
				->where('partai.NAMA_PARTAI','Hanura')
				->where('caleg.nama','like',$cari.'%')
				->select(
					'caleg_drh.email as email',
					'caleg_drh.telephone as no_telp',
					'caleg_drh.nama as nama',
					'provinsi.Nama_provinsi',
					'kabupaten.Nama_kabupaten',
					'caleg_drh.*'
				)->get();
		} else if($case == "DPD") {
			$dataCalon = DB::table('call_center_dpd')
				->leftjoin('provinsi','provinsi.id_provinsi','=','call_center_dpd.provinsi')
				->leftjoin('kabupaten','kabupaten.id_kabupaten','=','call_center_dpd.kabupaten')
				->where('caleg.nama_dpd','like',$cari.'%')
				->select(
					'call_center_dpd.email as email',
					'call_center_dpd.no_telp as no_telp',
					'call_center_dpd.nama_dpd as nama',
					'provinsi.Nama_provinsi',
					'kabupaten.Nama_kabupaten',
					'call_center_dpd.*'
				)->get();
		} else if($case == "DPC") {
			$dataCalon = DB::table('call_center_dpc')
				->join('kabupaten','kabupaten.id_kabupaten','=','call_center_dpc.kabupaten_kota')
				->join('provinsi','provinsi.id_provinsi','=','kabupaten.id_provinsi')
				->where('caleg.nama_ketua_dpc','like',$cari.'%')
				->select(
					'call_center_dpc.email as email',
					'call_center_dpc.no_telp as no_telp',
					'call_center_dpc.nama_ketua_dpc as nama',
					'provinsi.Nama_provinsi',
					'kabupaten.Nama_kabupaten',
					'call_center_dpc.*'
				)->get();
		}
		foreach ($dataCalon as $key) {
			
		}
	}
	public function viewDashboard($prov = 0)
	{
		$provnya = session()->get('idProvinsi2');
		$provsession = DB::table('m_geo_prov_kpu')
			->where('geo_prov_id', $provnya)
			->get();

		$dataProvAll = DB::table('m_geo_prov_kpu')
			->get();
		$dataProv = DB::table('m_geo_prov')
			->count();	
			

		if(isset($provnya)){
			$penduduka = DB::table('m_penduduk')
			->select(DB::raw('SUM(penduduk_jumlah) as penduduk'))
			->where('geo_prov_id', $provnya)
				->get();

			$pengurus = DB::table('m_pengurus')
			->where('geo_prov_id', $provnya)
			->get();

		}else{
			$penduduka = DB::table('m_penduduk')
				->select(DB::raw('SUM(penduduk_jumlah) as penduduk'))
				->get();

			$pengurus = DB::table('m_pengurus')
				->get();
		}


		if($prov != 0 ){
			$dataPendudukKab = DB::table('m_penduduk')
				->select(DB::raw('SUM(penduduk_jumlah) as jumlah_penduduk'))
					->where('geo_prov_id',$prov)
						->get();	

			$dataKabAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc) as dpc'))
				->where('geo_prov_id',$prov)
						->get();
			$dataKecAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac) as pac'))
				->where('geo_prov_id',$prov)
				->get();
			$dataKelAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting) as ranting'))
				->where('geo_prov_id',$prov)
				->get();
			$dataRWAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting) as a_ranting'))
				->where('geo_prov_id',$prov)
				->get();	
			$dataPendudukAll = DB::table('m_penduduk')
				->select(DB::raw('SUM(penduduk_jumlah) as jumlah_penduduk'))
				->where('geo_prov_id',$prov)
					->get();

			$dataKabV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc_ada) as dpc_ada'))
				->where('geo_prov_id',$prov)
				->get();	
			$dataKecV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac_ada) as pac_ada'))
				->where('geo_prov_id',$prov)
				->get();	
			$dataKelV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting_ada) as ranting_ada'))
				->where('geo_prov_id',$prov)
				->get();	
			$dataRWV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting_ada) as anak_ranting'))
				->where('geo_prov_id',$prov)
				->get();														
		} else {//--provinsi--

			$dataKabAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc) as dpc'))
				->get();

			$dataKecAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac) as pac'))
				->get();
			$dataKelAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting) as ranting'))
				->get();
			$dataRWAll = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting) as a_ranting'))
				->get();	
			$dataPendudukAll = DB::table('m_penduduk')
				->select(DB::raw('SUM(penduduk_jumlah) as jumlah_penduduk'))
					->get();

			$dataKabV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc_ada) as dpc_ada'))
				->get();	
			$dataKecV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac_ada) as pac_ada'))
				->get();	
			$dataKelV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting_ada) as ranting_ada'))
				->get();	
			$dataRWV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting_ada) as anak_ranting'))
				->get();											
		}
		
		
		$dataTotalPendudukAll = 0;
		$dataTotalPendudukKab = 0;
		
		foreach($dataPendudukAll as $tmp){
			$dataTotalPendudukAll = $tmp->jumlah_penduduk;
		}

		$jumlahPenduduk = 0;
		foreach ($penduduka as $tmp) {
			$jumlahPenduduk = $tmp->penduduk;
		}
		
		if($prov != 0){			
			foreach($dataPendudukKab as $tmp){
				$dataTotalPendudukKab = $tmp->jumlah_penduduk;
			}
			foreach($dataKabV as $tmp){
				$dataKabV = $tmp->dpc_ada;
			}
		} else {
			$dataTotalPendudukKab = $dataTotalPendudukAll;
			
			foreach($dataKabAll as $tmp){
				$dataKabAll = $tmp->dpc;
			}
			foreach($dataKecAll as $tmp){
				$dataKecAll = $tmp->pac;
			}
			foreach($dataKelAll as $tmp){
				$dataKelAll = $tmp->ranting;
			}
			foreach($dataRWAll as $tmp){
				$dataRWAll = $tmp->a_ranting;
			}

			foreach($dataKabV as $tmp){
				$dataKabV = $tmp->dpc_ada;
			}
			foreach($dataKecV as $tmp){
				$dataKecV = $tmp->pac_ada;
			}
			foreach($dataKelV as $tmp){
				$dataKelV = $tmp->ranting_ada;
			}
			foreach($dataRWV as $tmp){
				$dataRWV = $tmp->anak_ranting;
			}
		}

		$dataTotalPendudukAll =  number_format($dataTotalPendudukAll,0, "," , ".");
		if($dataKabV==0||$dataKabAll==0){
			$kabupatenV = "0.00";
		}else{
			$kabupatenV = number_format($dataKabV/$dataKabAll*100,2, "," , ".");
		}	
		if($dataKecV==0||$dataKecAll==0){
			$kecamatanV = "0.00";
		}else{
			$kecamatanV = number_format($dataKecV/$dataKecAll*100,2, "," , ".");
		}	
		if($dataKelV==0||$dataKelAll==0){
			$kelurahanV = "0.00";
		}else{
			$kelurahanV = number_format($dataKelV/$dataKelAll*100,2, "," , ".");
		}	
		if($dataRWV==0||$dataRWAll==0){
			$rwV = "0.00";
		}else{
			$rwV = number_format($dataRWV/$dataRWAll*100,2, "," , ".");
		}								
		
		
		$type = 'all';
		
		return view('main.dashboard.index', array(
			'type' => $type,
			'prov' => $prov,
			'dataProvAll' => $dataProvAll,
			'dataKabAll' => $dataKabAll,
			'dataKecAll' => $dataKecAll,
			'dataKelAll' => $dataKelAll,
			'dataRWAll' => $dataRWAll,
			'dataTotalPendudukAll' => $dataTotalPendudukAll,
			'dataTotalPendudukKab' => $dataTotalPendudukKab,
			'provsession' => $provsession,
			'jumlahPenduduk' => $jumlahPenduduk,
			'pengurus' => $pengurus,
			'dataKabV' => $dataKabV,
			'dataKecV' => $dataKecV,
			'dataKelV' => $dataKelV,
			'dataRWV' => $dataRWV,
			'kabupatenV' => $kabupatenV,
			'kecamatanV' => $kecamatanV,
			'kelurahanV' => $kelurahanV,
			'rwV' => $rwV

		));
	}
	
	public function viewDashboardGrafik($prov = 0,$pengurus = ''){
		$dates = "''";
		$datakursiall = "";
		$datakursialls = "";
		$datakursiallbelum = "";
		$totalall = "";
		$data_area = "";
		$data_series = ", series: [";
		$label = ",
		dataLabels: {
			enabled: true,
			rotation: -90,
			color: '#000000',
			align: 'right',
			format: '{point.y:f}', // one decimal
			y: -20, // 10 pixels down from the top
			style: {
				fontSize: '10px'
			}
		}";
		
		if($pengurus == '') {
			if (session('idProvinsi')) {
					$prov = session()->get('idProvinsi');
					$dataKabReal = DB::table('m_geo_kab')
						->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
							->where('m_geo_prov.geo_prov_id',$prov)
								->get();

					$jenis = [['KECAMATAN', '#000000'],['KELURAHAN','#FF0000']];
					
					foreach($dataKabReal as $tmp){
						$dates = $dates.",'".$tmp->geo_kab_nama."'";				
					}
					$dates = substr($dates, 3);
					
					$pengurus = DB::table('m_pengurus')
						->where('geo_prov_id', $prov)
						->get();

					for($a=0; $a < count($jenis); $a++){
						$data_series = $data_series."{
							name: '".$jenis[$a][0]."',
							color: '".$jenis[$a][1]."',
							data: [";
						
						if($jenis[$a][0] == 'KECAMATAN'){
								$jumlah_kursi = 0;
							foreach($dataKabReal as $tmp){
								$dataKursi =  DB::table('m_geo_kec')
									->where('geo_kab_id', $tmp->geo_kab_id)
									->count();
								$jumlah_kursi = $dataKursi;
								$datakursiall = $datakursiall.",".number_format($jumlah_kursi,1,'.','');
							}			
							$data_series = $data_series.substr($datakursiall, 1)."]".$label."},";
						} else if($jenis[$a][0] == 'KELURAHAN'){
							foreach($dataKabReal as $tmp){
								$jumlah_kursi = 0;
								$dataKursi = DB::table('m_geo_deskel')
									->join('m_geo_kec','m_geo_deskel.geo_kec_id','=','m_geo_kec.geo_kec_id')
									->join('m_geo_kab','m_geo_kec.geo_kab_id','=','m_geo_kab.geo_kab_id')
									->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
									->where('m_geo_kab.geo_kab_id',$tmp->geo_kab_id)
									->where('m_geo_prov.geo_prov_id',$prov)
										->count();
								$jumlah_kursi = $dataKursi;
								$datakursialls = $datakursialls.",".number_format($jumlah_kursi,1,'.','');
							}
							$data_series = $data_series.substr($datakursialls, 1)."]".$label."},";
						}
					} 				
			} else {
				if($prov == 0){
					$dataProvAll = DB::table('m_geo_prov')
						->get();
					foreach($dataProvAll as $tmp){
						$dates = $dates.",'".$tmp->geo_prov_nama."'";
					}

					$dates = substr($dates, 3);
					
					$jenis = [['KAB/KOTA', '#000000'],['KECAMATAN','#F39C12'],['KELURAHAN','#FF0000']];
					for($a=0; $a < count($jenis); $a++){
						$data_series = $data_series."{
							name: '".$jenis[$a][0]."',
							color: '".$jenis[$a][1]."',
							data: [";
						
						if($jenis[$a][0] == 'KAB/KOTA'){
							foreach($dataProvAll as $tmp){
								$jumlah_kursi = 0;
								$dataKab = DB::table('m_geo_kab')
									->where('geo_prov_id',$tmp->geo_prov_id)
										->count();
								$jumlah_kursi = $dataKab;
								$datakursiall = $datakursiall.",".number_format($jumlah_kursi,1,'.','');
							}
							$data_series = $data_series.substr($datakursiall, 1)."]".$label."},";				
						} else if($jenis[$a][0] == 'KELURAHAN'){
							foreach($dataProvAll as $tmp){
								$jumlah_kursi = 0;
								$dataKursi = DB::table('m_geo_deskel')
									->join('m_geo_kec','m_geo_deskel.geo_kec_id','=','m_geo_kec.geo_kec_id')
									->join('m_geo_kab','m_geo_kec.geo_kab_id','=','m_geo_kab.geo_kab_id')
									->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
									->where('m_geo_kab.geo_prov_id', $tmp->geo_prov_id)
										->count();
								$jumlah_kursi = $dataKursi;
								$datakursiallbelum = $datakursiallbelum.",".number_format($jumlah_kursi,1,'.','');
							}
							$data_series = $data_series.substr($datakursiallbelum, 1)."]".$label."},";
						} else if($jenis[$a][0] == 'KECAMATAN'){
							foreach($dataProvAll as $tmp){
								$jumlah_kursi = 0;
								
								$dataKursi = DB::table('m_geo_kec')
									->join('m_geo_kab','m_geo_kec.geo_kab_id','=','m_geo_kab.geo_kab_id')
									->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
									->where('m_geo_kab.geo_prov_id', $tmp->geo_prov_id)
										->count();
								$jumlah_kursi = $dataKursi;
								$datakursialls = $datakursialls.",".number_format($jumlah_kursi,1,'.','');
							}
							$data_series = $data_series.substr($datakursialls, 1)."]".$label."},";	
						}
					}
				} else {
					$dataKabReal = DB::table('m_geo_kab')
						->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
							->where('m_geo_prov.geo_prov_id',$prov)
								->get();

					$jenis = [['KECAMATAN', '#000000'],['KELURAHAN','#FF0000']];
					
					foreach($dataKabReal as $tmp){
						$dates = $dates.",'".$tmp->geo_kab_nama."'";				
					}
					$dates = substr($dates, 3);
					
					$pengurus = DB::table('m_pengurus')
						->where('geo_prov_id', '11')
						->get();

					for($a=0; $a < count($jenis); $a++){
						$data_series = $data_series."{
							name: '".$jenis[$a][0]."',
							color: '".$jenis[$a][1]."',
							data: [";
						
						if($jenis[$a][0] == 'KECAMATAN'){
								$jumlah_kursi = 0;
							foreach($dataKabReal as $tmp){
								$dataKursi =  DB::table('m_geo_kec')
									->where('geo_kab_id', $tmp->geo_kab_id)
										->count();
								$jumlah_kursi = $dataKursi;
								$datakursiall = $datakursiall.",".number_format($jumlah_kursi,1,'.','');
							}			
							$data_series = $data_series.substr($datakursiall, 1)."]".$label."},";
						} else if($jenis[$a][0] == 'KELURAHAN'){
							foreach($dataKabReal as $tmp){
								$jumlah_kursi = 0;
								$dataKursi = DB::table('m_geo_deskel')
									->join('m_geo_kec','m_geo_deskel.geo_kec_id','=','m_geo_kec.geo_kec_id')
									->join('m_geo_kab','m_geo_kec.geo_kab_id','=','m_geo_kab.geo_kab_id')
									->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
									->where('m_geo_kab.geo_kab_id',$tmp->geo_kab_id)
									->where('m_geo_prov.geo_prov_id',$prov)
										->count();
								$jumlah_kursi = $dataKursi;
								$datakursialls = $datakursialls.",".number_format($jumlah_kursi,1,'.','');
							}
							$data_series = $data_series.substr($datakursialls, 1)."]".$label."},";
						}
					} 
				}
			}
		} else {
			if (session('idProvinsi')) {
				$prov = session('idProvinsi');
			}
			$jenis = [['Terbentuk', '#000000'],['Total','#FF0000']];
			$dataPengurus = DB::table('m_pengurus')
				->where('geo_prov_id',$prov)
					->get();
					
			for($a=0; $a < count($jenis); $a++){
				$dates = $dates.",'Data ".strtoupper($pengurus)."'";
				
				if(count($dataPengurus) == 0) {
					$data_series = $data_series."{
						name: '".$jenis[$a][0]."',
						color: '".$jenis[$a][1]."',
						data: [";
					$data_series = $data_series."0]".$label."},";
				} else {			
					$data_series = $data_series."{
						name: '".$jenis[$a][0]."',
						color: '".$jenis[$a][1]."',
						data: [";
				
					foreach($dataPengurus as $tmp){
						if($pengurus == 'par'){					
							$nameColAda = 'pengurus_ranting';
							$nameCol = 'pengurus_ranting_ada';	
						} else {						
							$nameColAda = 'pengurus_'.$pengurus;
							$nameCol = 'pengurus_'.$pengurus.'_ada';
						}
						if($jenis[$a][0] == 'Terbentuk'){
							$jumlah_kursi = $tmp->$nameCol;
							$datakursiall = $datakursiall.",".number_format($jumlah_kursi,1,'.','');
							$data_series = $data_series.substr($datakursiall, 1)."]".$label."},";
						} else if($jenis[$a][0] == 'Total'){		
							$jumlah_kursi = $tmp->$nameColAda;
							$datakursialls = $datakursialls.",".number_format($jumlah_kursi,1,'.','');
							$data_series = $data_series.substr($datakursialls, 1)."]".$label."},";
						}
					}
				}
			} 
			$dates = substr($dates, 3);
		}
		return view('main.dashboard.grafik', array(
			'dates' => $dates,			
			'data_series' => $data_series."]"
		));
	}
	
	public function viewDashboardData($prov=0){
		if($prov == 0){
			$dataProv = DB::table('m_geo_prov')
				->count();	
			$dataKab =  DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc) as dpc'))
				->get();
			$dataKec = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac) as pac'))
				->get();
			$dataKel = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting) as ranting'))
				->get();;
			$dataRW = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting) as a_ranting'))
				->get();
			$dataPendudukKab = DB::table('m_penduduk')
				->select(DB::raw('SUM(penduduk_jumlah) as jumlah_penduduk'))
					->get();
			$dataKabV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc_ada) as dpc_ada'))
				->get();
			$dataKecV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac_ada) as pac_ada'))
				->get();	
			$dataKelV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting_ada) as ranting_ada'))
				->get();	
			$dataRWV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting_ada) as anak_ranting'))
				->get();				

		} else {
			$dataProv = 1;
			$dataKab =  DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc) as dpc'))
				->where('geo_prov_id',$prov)
				->get();
			$dataKec = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac) as pac'))
				->where('geo_prov_id',$prov)
				->get();
			$dataKel = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting) as ranting'))
				->where('geo_prov_id',$prov)
				->get();;
			$dataRW = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting) as a_ranting'))
				->where('geo_prov_id',$prov)
				->get();	
			$dataPendudukKab = DB::table('m_penduduk')
				->select(DB::raw('SUM(penduduk_jumlah) as jumlah_penduduk'))
					->where('geo_prov_id',$prov)
						->get();
			$dataKabV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_dpc_ada) as dpc_ada'))
				->where('geo_prov_id',$prov)
				->get();		
			$dataKecV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_pac_ada) as pac_ada'))
				->where('geo_prov_id',$prov)
				->get();	
			$dataKelV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_ranting_ada) as ranting_ada'))
				->where('geo_prov_id',$prov)
				->get();	
			$dataRWV = DB::table('m_pengurus')
				->select(DB::raw('SUM(pengurus_anak_ranting_ada) as anak_ranting'))
				->where('geo_prov_id',$prov)
				->get();								
		}
		
		$jmlPenduduk = 0;
		foreach($dataPendudukKab as $tmp){
			$jmlPenduduk = $tmp->jumlah_penduduk;
		}
		foreach($dataKabV as $tmp){
			$dataKabV = $tmp->dpc_ada;
		}
		foreach($dataKecV as $tmp){
			$dataKecV = $tmp->pac_ada;
		}
		foreach($dataKelV as $tmp){
			$dataKelV = $tmp->ranting_ada;
		}
		foreach($dataRWV as $tmp){
			$dataRWV = $tmp->anak_ranting;
		}		

		foreach($dataKab as $tmp){
			$dataKab = $tmp->dpc;
		}
		foreach($dataKec as $tmp){
			$dataKec = $tmp->pac;
		}		
		foreach($dataKel as $tmp){
			$dataKel = $tmp->ranting;
		}						
		foreach($dataRW as $tmp){
			$dataRW = $tmp->a_ranting;
		}	

		
		$data['prov'] = number_format($dataProv,0, "," , ".");
		$data['kab'] = number_format($dataKab,0, "," , ".");
		$data['kec'] = number_format($dataKec,0, "," , ".");
		$data['kel'] = number_format($dataKel,0, "," , ".");
		$data['rw'] = number_format($dataRW,0, "," , ".");
		$data['penduduk'] = number_format($jmlPenduduk,0, "," , ".");
		$data['kabv'] = number_format($dataKabV,0, "," , ".");
		$data['kecv'] = number_format($dataKecV,0, "," , ".");
		$data['kelv'] = number_format($dataKelV,0, "," , ".");
		$data['rwv'] = number_format($dataRWV,0, "," , ".");

		if($dataKabV==0||$dataKab==0){
			$data['Hkabv'] = "0.00";
		}else{
			$data['Hkabv'] = number_format($dataKabV/$dataKab*100,2, "," , ".");
		}
		if($dataKecV==0||$dataKec==0){
			$data['Hkecv'] = "0.00";
		}else{
			$data['Hkecv'] = number_format($dataKecV/$dataKec*100,2, "," , ".");
		}
		if($dataKelV==0||$dataKel==0){
			$data['Hkelv'] = "0.00";
		}else{
			$data['Hkelv'] = number_format($dataKelV/$dataKel*100,2, "," , ".");
		}
		if($dataRWV==0||$dataRW==0){
			$data['Hrwv'] = "0.00";
		}else{
			$data['Hrwv'] = number_format($dataRWV/$dataRW*100,2, "," , ".");
		}
		
		return json_encode($data);
	}
	
	
	public function filterSurvei(){
		$survei = @$_GET['survei'];
		if($survei != 'semua'){
			$dataSurvei = DB::table('call_center_dpc')
				->join('kabupaten','kabupaten.id_kabupaten','=','call_center_dpc.kabupaten_kota')
				->join('provinsi','provinsi.id_provinsi','=','call_center_dpc.provinsi')
				->leftjoin('call_center_dpd','call_center_dpd.provinsi','=','call_center_dpc.provinsi')
					->where('call_center_dpc.presentase','!=','0')
					->where('call_center_dpc.responden','!=','')
					->where('provinsi.Nama_provinsi',$survei)
						->select(
							'call_center_dpc.email as email',
							'call_center_dpc.no_telp as no_telp',
							'call_center_dpc.nama_ketua_dpc',
							'call_center_dpd.nama_dpd',
							'call_center_dpd.kabupaten',
							'call_center_dpd.provinsi',
							'call_center_dpc.presentase',
							'provinsi.Nama_provinsi',
							'kabupaten.Nama_kabupaten'
						)->groupBy('kode_dpc')
							->get();
			$type = $survei;
		}else{
			$dataSurvei = DB::table('call_center_dpc')
				->join('kabupaten','kabupaten.id_kabupaten','=','call_center_dpc.kabupaten_kota')
				->join('provinsi','provinsi.id_provinsi','=','call_center_dpc.provinsi')
				->leftjoin('call_center_dpd','call_center_dpd.provinsi','=','call_center_dpc.provinsi')
					->where('call_center_dpc.presentase','!=','0')
					->where('call_center_dpc.responden','!=','')
						->select(
							'call_center_dpc.email as email',
							'call_center_dpc.no_telp as no_telp',
							'call_center_dpc.nama_ketua_dpc',
							'call_center_dpd.nama_dpd',
							'call_center_dpd.kabupaten',
							'call_center_dpd.provinsi',
							'call_center_dpc.presentase',
							'provinsi.Nama_provinsi',
							'kabupaten.Nama_kabupaten'
						)->groupBy('kode_dpc')
							->get();
			$type = 'semua';
		}
			return response()->view('data_survei', array(
				'dataSurvei' => $dataSurvei,
				'type' => $type
			));			
	}
}