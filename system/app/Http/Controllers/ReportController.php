<?php namespace App\Http\Controllers; 

use DB;
use Input;
use Redirect;
use Fpdf; 

/**
* 
*/
class ReportController extends Controller
{
	public function viewReportPefoma($type,$prov=null,$kab=null,$kec=null,$deskel=null,$rw=null){
		$namaKetuaDPD = '';
		$namaSekDPD = '';
		$SkepKet = '';
		$SkepSek = '';
		$breadcrumb[] = 'Report';
		
		$provinsi = DB::table('m_geo_prov_kpu')->get();
		$dataProv = DB::table('m_geo_prov_kpu')
			->where('geo_prov_id',$prov)
				->first();
		
		if($type == 'dashboard') {
			$breadcrumb[]='Dashboard Report';
		} else if($type == 'dpd') {
			$breadcrumb[]='Perfoma DPD';
		} else if($type == 'dpc') {
			$breadcrumb[]='Perfoma DPD Terhadap Pembentukan DPC';
		} else if($type == 'pac') {
			$breadcrumb[]='Perfoma DPD Terhadap Pembentukan PAC';
			
		} else if($type == 'pr') {
			$breadcrumb[]='Perfoma DPD Terhadap Pembentukan PR';
			
		} else if($type == 'par') {
			$breadcrumb[]='Perfoma DPD Terhadap Pembentukan PAR';
			
		} else if($type == 'kpa') {
			$breadcrumb[]='Perfoma DPD Terhadap Pembentukan KPA';
			
		}
		
		$masterData = [
			'type' => $type,
			'provinsi' => $provinsi,
			'dataAnggota' => @$dataAnggota,
			'dataLingkar' => @$dataLingkar,
			'selected' =>[$prov,$kab,$kec,$deskel,$rw],
			'breadcrumb' => $breadcrumb
		];
		if($kab)
			$masterData['kabupaten'] = DB::table('m_geo_kab_kpu')
				->select('geo_kab_nama','geo_kab_id')
					->where('geo_prov_id','=',$prov)
						->get();
		if($kec)
			$masterData['kecamatan'] = DB::table('m_geo_kec_kpu')
				->select('geo_kec_nama','geo_kec_id')
					->where('geo_kab_id','=',$kab)
						->get();
		if($deskel)
			$masterData['kelurahan'] = DB::table('m_geo_deskel_kpu')
				->select('geo_deskel_nama','geo_deskel_id')
					->where('geo_kec_id','=',$kec)
						->get();
		if($rw)
			$masterData['rukunwarga'] = DB::table('m_geo_rw_kpu')
				->select('geo_rw_nama','geo_rw_id')
					->where('geo_deskel_id','=',$deskel)
						->get();
						
		$dataDPD = DB::table('m_bio')
			->join('r_bio_dpd','r_bio_dpd.bio_id','=','m_bio.bio_id')
			->leftjoin('m_struk_dpd','m_struk_dpd.struk_dpd_id','=','r_bio_dpd.struk_dpd_id')
				->where('m_struk_dpd.struk_dpd_nama','=',"Ketua")
				->where('r_bio_dpd.geo_prov_id',$prov)
					->get();

		$dataDPDS = DB::table('m_bio')
			->join('r_bio_dpd','r_bio_dpd.bio_id','=','m_bio.bio_id')
			->leftjoin('m_struk_dpd','m_struk_dpd.struk_dpd_id','=','r_bio_dpd.struk_dpd_id')
				->where('m_struk_dpd.struk_dpd_nama','=',"Sekretaris")
				->where('r_bio_dpd.geo_prov_id',$prov)
					->get();
			
		foreach($dataDPD as $tmp){
			$namaKetuaDPD = $tmp->bio_nama_depan.' '.$tmp->bio_nama_tengah.' '.$tmp->bio_nama_belakang;
			$SkepKet = $tmp->bio_dpd_sk;
		}
				
		foreach($dataDPDS as $tmp){
			$namaSekDPD = $tmp->bio_nama_depan.' '.$tmp->bio_nama_tengah.' '.$tmp->bio_nama_belakang;
			$SkepSek = $tmp->bio_dpd_sk;
		}

		$masterData['namaKetuaDPD'] = $namaKetuaDPD;
		$masterData['namaSekDPD'] = $namaSekDPD;
		$masterData['SkepKet'] = $SkepKet;
		$masterData['SkepSek'] = $SkepSek;
		$masterData['namaDaerah'] = @$dataProv->geo_prov_nama;
		
		return view('main.report.index',$masterData);
	}
	
	public function viewGrafikReportPefoma($jenis,$type,$prov=null,$kab=null,$kec=null,$deskel=null,$rw=null,$dprdi=null){
		$data = [];
		$dataProv = DB::table('m_geo_prov_kpu')
			->where('geo_prov_id',$prov)
				->first();
		
		if($prov) {			
			$pengurus = DB::table('m_pengurus')
				->where('geo_prov_id',$prov)
					->first();

			$performa = DB::select('select 
			statistik_dapil_t1 as dapil_t1, statistik_dapil_t2 as dapil_t2, statistik_dapil_t3 as dapil_t3, 
			statistik_kursi_t1 as kursi_t1, statistik_kursi_t2 as kursi_t2, statistik_kursi_t3 as kursi_t3, 
			(select count(*) from r_bio_dprri join m_bio on m_bio.bio_id = r_bio_dprri.bio_id where r_bio_dprri.geo_prov_id = m_statistik_kursi.geo_prov_id) as kursi_t1_ada,
			(select count(*) from r_bio_dprdi join m_bio on m_bio.bio_id = r_bio_dprdi.bio_id where r_bio_dprdi.geo_prov_id = m_statistik_kursi.geo_prov_id) as kursi_t2_ada,
			(select count(*) from r_bio_dprdii join m_bio on m_bio.bio_id = r_bio_dprdii.bio_id where r_bio_dprdii.geo_prov_id = m_statistik_kursi.geo_prov_id) as kursi_t3_ada
			FROM m_statistik_kursi 
			JOIN m_geo_prov_kpu ON m_geo_prov_kpu.geo_prov_id = m_statistik_kursi.geo_prov_id
			WHERE m_geo_prov_kpu.geo_prov_id="'.$prov.'"
			GROUP BY m_statistik_kursi.geo_prov_id');
			/*->join('m_geo_prov_kpu',' m_geo_prov_kpu.geo_prov_id','=','m_statistik_kursi.geo_prov_id')*/
			/*->where('m_geo_prov_kpu.geo_prov_id',$prov) */  
			/*->groupBy('m_statistik_kursi.geo_prov_id');*/
		} else {
			$pengurus = DB::table('m_pengurus')
				->select(DB::raw('sum(pengurus_dpc) as pengurus_dpc'),DB::raw('sum(pengurus_dpc_ada) as pengurus_dpc_ada')
				,DB::raw('sum(pengurus_pac) as pengurus_pac'),DB::raw('sum(pengurus_pac_ada) as pengurus_pac_ada')
				,DB::raw('sum(pengurus_ranting) as pengurus_ranting'),DB::raw('sum(pengurus_ranting_ada) as pengurus_ranting_ada')
				,DB::raw('sum(pengurus_anak_ranting) as pengurus_anak_ranting'),DB::raw('sum(pengurus_anak_ranting_ada) as pengurus_anak_ranting_ada')
				,DB::raw('sum(pengurus_kpa) as pengurus_kpa'),DB::raw('sum(pengurus_kpa_ada) as pengurus_kpa_ada'))
					->first();

			$performa = DB::select('select 
			sum(statistik_dapil_t1) as dapil_t1, sum(statistik_dapil_t2) as dapil_t2, sum(statistik_dapil_t3) as dapil_t3, 
			sum(statistik_kursi_t1) as kursi_t1, sum(statistik_kursi_t2) as kursi_t2, sum(statistik_kursi_t3) as kursi_t3, 
			(select count(*) from r_bio_dprri join m_bio on m_bio.bio_id = r_bio_dprri.bio_id) as kursi_t1_ada,
			(select count(*) from r_bio_dprdi join m_bio on m_bio.bio_id = r_bio_dprdi.bio_id) as kursi_t2_ada,
			(select count(*) from r_bio_dprdii join m_bio on m_bio.bio_id = r_bio_dprdii.bio_id) as kursi_t3_ada
			FROM m_statistik_kursi');

			/*->join('m_geo_prov_kpu',' m_geo_prov_kpu.geo_prov_id','=','m_statistik_kursi.geo_prov_id')*/
			/*->where('m_geo_prov_kpu.geo_prov_id',$prov) */  
			/*->groupBy('m_statistik_kursi.geo_prov_id');*/
		}

		foreach ($performa as $t) {
			$dataKursit1 = $t->kursi_t1;
			$dataKursit1ada = $t->kursi_t1_ada;
			$dataKursit2 = $t->kursi_t2;
			$dataKursit2ada = $t->kursi_t2_ada;
			$dataKursit3 = $t->kursi_t3;
			$dataKursit3ada = $t->kursi_t3_ada;

			if($dataKursit1ada != 0 || $dataKursit1 != 0){
				$dataDprriPer = number_format($dataKursit1ada/$dataKursit1*100,2) ;
			}
			if($dataKursit2ada != 0 || $dataKursit2 != 0){
				$dataDprdiPer = number_format($dataKursit2ada/$dataKursit2*100,2);
			}
			if($dataKursit3ada != 0 || $dataKursit3 != 0){
				$dataDprdiiPer = number_format($dataKursit3ada/$dataKursit3*100,2);
			}		
		}
		
		

		if($type == 'pie'){
			if($jenis == 'dpd'){
				$dataGrafik = [['Sudah Ada',1],['Belum Ada',0]];
			} else if($jenis == 'dpc'){
				$dataGrafik = [['Sudah Ada',@$pengurus->pengurus_dpc_ada],['Belum Ada',@$pengurus->pengurus_dpc-@$pengurus->pengurus_dpc_ada]];
			} else if($jenis == 'pac'){
				$dataGrafik = [['Sudah Ada',@$pengurus->pengurus_pac_ada],['Belum Ada',@$pengurus->pengurus_pac-@$pengurus->pengurus_pac_ada]];
			} else if($jenis == 'pr'){
				$dataGrafik = [['Sudah Ada',@$pengurus->pengurus_ranting_ada],['Belum Ada',@$pengurus->pengurus_ranting-@$pengurus->pengurus_ranting_ada]];
			} else if($jenis == 'par'){
				$dataGrafik = [['Sudah Ada',@$pengurus->pengurus_anak_ranting_ada],['Belum Ada',@$pengurus->pengurus_ranting-@$pengurus->pengurus_ranting_ada]];
			} else if($jenis == 'kpa'){
				$dataGrafik = [['Sudah Ada',@$pengurus->pengurus_kpa_ada],['Belum Ada',@$pengurus->pengurus_kpa-@$pengurus->pengurus_kpa_ada]];
			}
			
			
			$return = 'main.report.pie';
			
		} else if($type == 'ketua'){
			
		} else if($type == 'provinsi'){
			$return = 'main.report.line';
			$dataProv = DB::table('m_geo_prov_kpu')
				->count();
			$dataDPD = DB::table('m_bio')
				->join('r_bio_dpd','r_bio_dpd.bio_id','=','m_bio.bio_id')
				->leftjoin('m_struk_dpd','m_struk_dpd.struk_dpd_id','=','r_bio_dpd.struk_dpd_id')
					->where('m_struk_dpd.struk_dpd_nama','Ketua')
						->count();

			$dataProvPer = number_format(@$dataDPD/@$dataProv*100,2);
			$data['dataProvPer'] = $dataProvPer;
			/* $dataGrafik = [['Provinsi',count($dataProv)],['DPC B.TBK',@$dataProv@$pengurus->pengurus_dpc-@$pengurus->pengurus_dpc_ada],['DPC TBK',@$pengurus->pengurus_dpc_ada]]; */
			$dataGrafik = [['Provinsi',@$dataProv],['DPD B.TBK',@$dataProv-@$dataDPD],['DPD TBK', @$dataDPD]];
		} else if($type == 'kabupaten'){
			$return = 'main.report.line';
			$dataKab = DB::table('m_geo_kab')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
					->where('m_geo_prov.geo_prov_id',$prov);

			$dataKabPer = number_format(@$pengurus->pengurus_dpc_ada/@$pengurus->pengurus_dpc*100,2);
			$data['dataKabPer'] = $dataKabPer;
			/* $dataGrafik = [['Kabupaten',@$pengurus->pengurus_dpc],['DPC B.TBK',@$pengurus->pengurus_dpc-@$pengurus->pengurus_dpc_ada],['DPC TBK',@$pengurus->pengurus_dpc_ada]]; */
			$dataGrafik = [['Kabupaten',@$pengurus->pengurus_dpc],['DPC B.TBK',@$pengurus->pengurus_dpc-@$pengurus->pengurus_dpc_ada],['DPC TBK',@$pengurus->pengurus_dpc_ada]];
		} else if($type == 'kecamatan'){
			$return = 'main.report.line';
			$dataKec = DB::table('m_geo_kec')
				->join('m_geo_kab','m_geo_kab.geo_kab_id','=','m_geo_kec.geo_kab_id')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
					->where('m_geo_prov.geo_prov_id',$prov);
					if($kab){
						$dataKec->where('m_geo_kab.geo_kab_id',$kab);
					}
			$dataKecPer = number_format(@$pengurus->pengurus_pac_ada/@$pengurus->pengurus_pac*100,2);
			$data['dataKecPer'] = $dataKecPer;		
			/* $dataGrafik = [['Kecamatan',@$pengurus->pengurus_pac],['PAC B.TBK',@$pengurus->pengurus_pac-@$pengurus->pengurus_pac_ada],['PAC TBK',@$pengurus->pengurus_pac_ada]]; */
			$dataGrafik = [['Kecamatan',@$pengurus->pengurus_pac],['PAC B.TBK',@$pengurus->pengurus_pac-@$pengurus->pengurus_pac_ada],['PAC TBK',@$pengurus->pengurus_pac_ada]];
		} else if($type == 'kelurahan'){
			$return = 'main.report.line';
			$dataKel = DB::table('m_geo_deskel')
				->join('m_geo_kec','m_geo_kec.geo_kec_id','=','m_geo_deskel.geo_kec_id')
				->join('m_geo_kab','m_geo_kab.geo_kab_id','=','m_geo_kec.geo_kab_id')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
					->where('m_geo_prov.geo_prov_id',$prov);
					if($kab){
						$dataKel->where('m_geo_kab.geo_kab_id',$kab);
					}
					if($kec){
						$dataKel->where('m_geo_kec.geo_kec_id',$kec);
					}
			$dataKelPer = number_format(@$pengurus->pengurus_ranting_ada/@$pengurus->pengurus_ranting*100,2);
			$data['dataKelPer'] = $dataKelPer;			
			/* $dataGrafik = [['Kelurahan',@$pengurus->pengurus_ranting],['PR B.TBK',@$pengurus->pengurus_ranting-@$pengurus->pengurus_ranting_ada],['PR TBK',@$pengurus->pengurus_ranting_ada]]; */
			$dataGrafik = [['Kelurahan',@$pengurus->pengurus_ranting],['PR B.TBK',@$pengurus->pengurus_ranting-@$pengurus->pengurus_ranting_ada],['PR TBK',@$pengurus->pengurus_ranting_ada]];
		} else if($type == 'rw'){
			$return = 'main.report.line';
			$dataRW = DB::table('m_geo_rw')
				->join('m_geo_deskel','m_geo_deskel.geo_deskel_id','=','m_geo_rw.geo_deskel_id')
				->join('m_geo_kec','m_geo_kec.geo_kec_id','=','m_geo_deskel.geo_kec_id')
				->join('m_geo_kab','m_geo_kab.geo_kab_id','=','m_geo_kec.geo_kab_id')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
					->where('m_geo_prov.geo_prov_id',$prov);
					if($kab){
						$dataRW->where('m_geo_kab.geo_kab_id',$kab);
					}
					if($kec){
						$dataRW->where('m_geo_kec.geo_kec_id',$kec);
					}
					if($deskel){
						$dataRW->where('m_geo_deskel.geo_deskel_id',$deskel);
					}
			$dataRwPer = number_format(@$pengurus->pengurus_anak_ranting_ada/@$pengurus->pengurus_anak_ranting*100,2);
			$data['dataRwPer'] = $dataRwPer;			
			/* $dataGrafik = [['RW',@$pengurus->pengurus_anak_ranting],['PAR B.TBK',@$pengurus->pengurus_anak_ranting-@$pengurus->pengurus_anak_ranting_ada],['PAR TBK',@$pengurus->pengurus_anak_ranting_ada]]; */
			$dataGrafik = [['RW',@$pengurus->pengurus_anak_ranting],['PAR B.TBK',@$pengurus->pengurus_anak_ranting-@$pengurus->pengurus_anak_ranting_ada],['PAR TBK',@$pengurus->pengurus_anak_ranting_ada]];
		} else if($type == 'rt'){
			$return = 'main.report.line';
			$dataRT = DB::table('m_geo_rt')
				->join('m_geo_rw','m_geo_rw.geo_rw_id','=','m_geo_rt.geo_rw_id')
				->join('m_geo_deskel','m_geo_deskel.geo_deskel_id','=','m_geo_rw.geo_deskel_id')
				->join('m_geo_kec','m_geo_kec.geo_kec_id','=','m_geo_deskel.geo_kec_id')
				->join('m_geo_kab','m_geo_kab.geo_kab_id','=','m_geo_kec.geo_kab_id')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','m_geo_kab.geo_prov_id')
					->where('m_geo_prov.geo_prov_id',$prov);
					if($kab){
						$dataRT->where('m_geo_kab.geo_kab_id',$kab);
					}
					if($kec){
						$dataRT->where('m_geo_kec.geo_kec_id',$kec);
					}
					if($deskel){
						$dataRT->where('m_geo_deskel.geo_deskel_id',$deskel);
					}
					if($rw){
						$dataRT->where('m_geo_rw.geo_rw_id',$rw);
					}
			$dataRtPer = number_format(@$pengurus->pengurus_kpa_ada/@$pengurus->pengurus_kpa*100,2);
			$data['dataRtPer'] = $dataRtPer;				
			/* $dataGrafik = [['RT',@$pengurus->pengurus_kpa],['KPA B.TBK',@$pengurus->pengurus_kpa-@$pengurus->pengurus_kpa_ada],['KPA TBK',@$pengurus->pengurus_kpa_ada]]; */
			$dataGrafik = [['RT',@$pengurus->pengurus_kpa],['KPA B.TBK',@$pengurus->pengurus_kpa-@$pengurus->pengurus_kpa_ada],['KPA TBK',@$pengurus->pengurus_kpa_ada]];
		} else if($type == 'dprri'){
			$return = 'main.report.line';
			$dataDprdi = DB::table('r_bio_dprri')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','r_bio_dprri.geo_prov_id')
				->join('m_bio','m_bio.bio_id','=','r_bio_dprri.bio_id')
					->where('m_geo_prov.geo_prov_id',$prov);

			/* $dataGrafik = [['Kabupaten',@$pengurus->pengurus_dpc],['DPC B.TBK',@$pengurus->pengurus_dpc-@$pengurus->pengurus_dpc_ada],['DPC TBK',@$pengurus->pengurus_dpc_ada]]; */
			foreach ($performa as $d) {
				$dataGrafik = [['Jumlah Kursi',$d->kursi_t1],['DPRD I',$d->kursi_t1-$d->kursi_t1_ada],['DPRD I',$d->kursi_t1_ada]];
			}
			
		} else if($type == 'dprdi'){
			$return = 'main.report.line';
			$dataDprdi = DB::table('r_bio_dprdi')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','r_bio_dprdi.geo_prov_id')
				->join('m_bio','m_bio.bio_id','=','r_bio_dprdi.bio_id')
					->where('m_geo_prov.geo_prov_id',$prov);

			/* $dataGrafik = [['Kabupaten',@$pengurus->pengurus_dpc],['DPC B.TBK',@$pengurus->pengurus_dpc-@$pengurus->pengurus_dpc_ada],['DPC TBK',@$pengurus->pengurus_dpc_ada]]; */
			foreach ($performa as $d) {
				$dataGrafik = [['Jumlah Kursi',$d->kursi_t2],['DPRD I',$d->kursi_t2-$d->kursi_t2_ada],['DPRD I',$d->kursi_t2_ada]];
			}
			
		} else if($type == 'dprdii'){
			$return = 'main.report.line';
			$dataDprdii = DB::table('r_bio_dprdii')
				->join('m_geo_prov','m_geo_prov.geo_prov_id','=','r_bio_dprdii.geo_prov_id')
				->join('m_bio','m_bio.bio_id','=','r_bio_dprdii.bio_id')
					->where('m_geo_prov.geo_prov_id',$prov);
					
			/* $dataGrafik = [['Kabupaten',@$pengurus->pengurus_dpc],['DPC B',@$pengurus->pengurus_dpc-@$pengurus->pengurus_dpc_ada],['DPC',@$pengurus->pengurus_dpc_ada]]; */
			foreach ($performa as $d) {
				$dataGrafik = [['Jumlah Kursi',$d->kursi_t3],['DPRD II',$d->kursi_t3-$d->kursi_t3_ada],['DPRD II',$d->kursi_t3_ada]];
			}
		}
		
		
		if($type == 'pie'){			
			$dataSeries = ",series: [{
				name: 'Brands',
				colorByPoint: true,
				data: [{
					name: '".@$dataGrafik[0][0]."',
					color: '#f39c12',
					y: ".@$dataGrafik[0][1]."
				},{
					name: '".@$dataGrafik[1][0]."',
					color: '#ff0000',
					y: ".@$dataGrafik[1][1]."
				}]
			}]";
		} else {			
			$dataSeries = ",series: [{
					name: '".@$dataGrafik[0][0]."',
					color: '#000000',
					data: [".@$dataGrafik[0][1]."]
				}, {
					name: '".@$dataGrafik[2][0]."',
					color: '#f39c12',
					data: [".@$dataGrafik[2][1]."]
				}]";
		}
						
		$data['dataSeries'] = $dataSeries;
		$data['dataDprriPer'] = $dataDprriPer;
		$data['dataDprdiPer'] = $dataDprdiPer;
		$data['dataDprdiiPer'] = $dataDprdiiPer;
		$data['namaDaerah'] = @$dataProv->geo_prov_nama;
		
		$data['type'] = $type;
		$data['jenis'] = $jenis;
		return view($return,$data);
	}
	
}